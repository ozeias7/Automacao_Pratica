Dir[File.join(File.dirname(__FILE__), "../pages/*_pages.rb")].each {|file| require file}

module Pages
    
     def assinar
         @assinar ||= AssinarEm.new
     end

     def  autenticacao
          @autenticacao ||= Autenticacao.new
     end

     def login
         @login ||= Login.new 
     end

     def usercadastro
         @usercadastro ||= UserCadastro.new       
     end
    
     def menucompras
         @menucompras ||= MenuCompras.new        
     end

     def categoria
         @categoria ||= Categoria.new       
     end

     def produto
         @produto ||= Produto.new
     end
  
     def categoriapopup
         @categoriapopup ||= CategoriaPopUp.new
     end

     def resumo
         @resumo ||= Resumo.new
     end

     def endereco
         @endereco ||= Endereco.new
     end

     def expedicao
         @expedicao ||= Expedicao.new
     end
    
     def pagamento
         @pagamento ||= Pagamento.new
     end

     def confirmarpagamento
         @confirmarpagamento ||= ConfirmarPagamento.new
     end
        
     def comprasucesso
         @comprasucesso ||= CompraSucesso.new      
     end


     def desconectar
         @desconectar ||= Desconectar.new
     end
end
