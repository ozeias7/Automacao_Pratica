class AssinarEm <SitePrism::Page

   set_url '/index.php'

   #Elementos

    element :assinar, :xpath, '//*[@id="header"]/div[2]/div/div/nav/div[1]/a'
  
    # Métodos

    def btassinar
        assinar.click
    end
end