class Desconectar <SitePrism::Page
    
    set_url '/index.php?controller=my-account'
    
    
    # Elementos

    element :sair, :xpath, '//*[@id="header"]/div[2]/div/div/nav/div[2]/a'
    

    # Metodos

    def deslogar
        sair.click  
    end
end