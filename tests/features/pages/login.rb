class Login <SitePrism::Page

    set_url '/index.php?controller=authentication&back=my-account'

   # Elementos

   element :email,'#email'
   element :senha,'#passwd'
   element :assinar, '#SubmitLogin'


   def loginusuario(login)
    email.set login['emailLogin'] 
    senha.set login['senhaLogin']
   end

   def assinarem
    assinar.click
   end


end