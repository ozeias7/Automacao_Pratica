class Categoria <SitePrism::Page

    set_url '/index.php?id_category=5&controller=category'
    
    # Elementos

    element :categoria, :xpath, '//div[@class="right-block"]/div[@class="button-container"]'
    
    # Métodos
    
    def selecionarcategoria
        categoria.click  
    end

end

