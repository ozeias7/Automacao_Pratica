class MenuCompras <SitePrism::Page

    set_url '/index.php?id_category=5&controller=category'

    # Elementos

    element :Mulheres, :xpath, '//*[@id="block_top_menu"]/ul/li[1]/a'
    element :vestidos, :xpath, '//*[@id="block_top_menu"]/ul/li[1]/a'
    element :camisetas, :xpath, '//*[@id="block_top_menu"]/ul/li[3]/a'

    
    # Métodos

    def selecionacamisetas
        camisetas.click
    end
end

