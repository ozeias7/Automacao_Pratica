class Autenticacao <SitePrism::Page

    set_url '/index.php?controller=authentication&back=my-account'

    # Elementos

   element :emailcadastro, '#email_create'
   element :creiasuacontaaqui,'#SubmitCreate'
   
   # Métodos

    def email(autenticacao)
        emailcadastro.set autenticacao['emailcadastro']   
    end

    def cliqueaqui
        creiasuacontaaqui.click
    end

    
end