class Resumo <SitePrism::Page

    set_url '//index.php?controller=order'

     # Elementos

    element :fazercheckout, :xpath,'//*[@id="center_column"]/p[2]/a[1]'
   
    
    # Métodos

    def realizecheckout
        fazercheckout.click
    end
end