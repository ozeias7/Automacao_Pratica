class UserCadastro <SitePrism::Page 
    
    set_url '/index.php?controller=authentication&back=my-account#account-creation'

    # Elementos

    element :sr,'#id_gender1'
    element :sra,'#id_gender2'
    element :primeironome, '#customer_firstname'
    element :sobrenome,'#customer_lastname'
    element :senha,'#passwd'
    element :endprimeironome,'#firstname'
    element :endsobrenome,'#lastname'
    element :endereco,'#address1'
    element :cidade,'#city'
    element :combo_estado,'#uniform-id_state'
    element :combo_selecionado, :xpath, '//*[@id="id_state"]/option[2]'
    element :cep,'#postcode' 
    element :combo_pais,'#id_country'
    element :telefonecelular,'#phone_mobile'
    element :enderecoalternativo,'#alias'
    element :registro, '#submitAccount'


    # Metodos
    
    def cadastrar_Usuario(usercadastro)
        primeironome.set usercadastro['primeironome']
        sobrenome.set usercadastro['sobrenome']
        senha.set usercadastro['senha']
        endereco.set usercadastro['endereco']
        cidade.set usercadastro['cidade']
        cep.set usercadastro['cep']
        telefonecelular.set usercadastro['telefonecelular']
        enderecoalternativo.set usercadastro['enderecoalternativo']
    end

    def selecionarEstado
        combo_estado.click
    end 

    def estadoselecionado
        combo_selecionado.click
    end

    def seleciobarPais(pais)
        combo_pais.find('option', text: pais).Select_option
    end
   
    def criarUsuario
        registro.click
    end

end