class Endereco <SitePrism::Page

    set_url '/index.php?controller=order&step=1'

    # Elementos

    
     element :endereço, :xpath, '//*[@id="center_column"]/form/p/button'


    # Métodos

    def fazercheckout_endereço
        endereço.click
    end
end