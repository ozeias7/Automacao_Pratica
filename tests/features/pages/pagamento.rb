class Pagamento <SitePrism::Page

   set_url '/index.php?controller=order&multi-shipping='

   # Elementos
     
      element :transferencia, :xpath, '//*[@id="HOOK_PAYMENT"]/div[1]/div/p/a'
      
      
      element :cheque, '.cheque'


   # Métodos

    def transferenciabancaria
        transferencia.click
    end 

    def pagamentocheque
        cheque.click
    end
end