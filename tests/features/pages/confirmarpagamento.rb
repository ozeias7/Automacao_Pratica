class ConfirmarPagamento <SitePrism::Page
    
     set_url '/index.php?fc=module&module=bankwire&controller=payment'

    # Elementos

    element :pedido, :xpath, '//*[@id="cart_navigation"]/button'

    element :meiospagamento, '.button-exclusive btn btn-default'



    # Métodos

    def confirmarpedido 
        pedido.click
    end

    def outromeiopagamento
        meiospagamento.click
    end



end