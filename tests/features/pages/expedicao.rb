class Expedicao <SitePrism::Page

    set_url '/index.php?controller=order'

    # Elementos
        
       element :concordo, '.checkbox'
       element :fazerexpedicao, :xpath, '//*[@name="processCarrier"]'


     # Metodos
    def aceito
        concordo.click
    end

    def btnexpedicao
        fazerexpedicao.click
    end
end