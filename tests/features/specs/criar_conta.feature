#language:pt

@Cadastro
Funcionalidade: Cadastrar Usuário

- Eu como cliente
- Quero criar uma conta no site


Cenario: Cadastrar usuario sem informar primeiro nome
Quando eu clico no login
E preenchemos os dados da criação da conta:
|emailcadastro     |
|teste76t@gmail.com|
E clico no botão criar
E eu informo o dados:
|primeironome|sobrenome |senha  |endereco|cidade|cep  |telefonecelular|enderecoalternativo|     
| Teste      |nascimento|123456 |SP      |BR    |36265|12345678       |Testes             |
E clico no botão estado
E clico no botão criar usuário
E clico no botão Deslogar



