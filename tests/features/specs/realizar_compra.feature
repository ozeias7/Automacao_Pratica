#language:pt

@Compra_produto
Funcionalidade: Realizar compra de produto

- Eu como cliente
- Quero comprar um produto


Cenario: Realizar compra de produto
Dado eu clico no login
E eu informo os dados:
|emailLogin        |senhaLogin |
|teste76t@gmail.com|123456     |
E clico no botão Assinar Em
E seleciono a opção de camisetas
E clico no botão adicionar ao carrinho
E clico no botão avançar para o checkout
E clico no botão fazer checkout
E clico no botão endereço
E clico no botão aceito
E clico no botão fazer expedição
E clico no botão transferência
E clico no botão confirmar pedido


