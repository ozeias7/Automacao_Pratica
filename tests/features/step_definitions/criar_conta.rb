Quando("eu clico no login") do
  assinar.load
  find(:xpath,'//*[@id="header"]/div[2]/div/div/nav/div[1]/a').click
  assinar.btassinar
  sleep(1)
end                                                                                                                                                            

Quando("preenchemos os dados da criação da conta:") do |table|                        
  autenticacao.email(table.hashes[0])
  sleep(3)
end                                                                                   
 
Quando("clico no botão criar") do
  autenticacao.cliqueaqui
  sleep(3)  
end
                                                                                                                                                                                                                                    
Quando("eu informo o dados:") do |table|                            
  usercadastro.cadastrar_Usuario(table.hashes[0])
  sleep(5)
end  

Quando("clico no botão estado") do
  usercadastro.selecionarEstado
  usercadastro.estadoselecionado
  sleep(5) 
end
                                                                                                                                                                                                                                              
Quando("clico no botão criar usuário") do                                         
  usercadastro.criarUsuario
  sleep(8)
end                                                                               
                                                                                  
Quando("clico no botão Deslogar") do
  desconectar.deslogar
  sleep (5)
end                                                    