Quando("eu informo os dados:") do |table|   
  login.load   
  login.loginusuario(table.hashes[0])
  sleep(2) 
end                                                                                              
                                                                                                 
Quando("clico no botão Assinar Em") do                                                           
  login.assinarem
  sleep (2)
end                                                                                              
                                                                                                 
Quando("seleciono a opção de camisetas") do                                                      
  menucompras.selecionacamisetas
  sleep(2)
end                                                                                              
                                                                                                                                                                                                       
Dado("clico no botão adicionar ao carrinho") do
  categoria.selecionarcategoria
  sleep(3)
end

Dado("clico no botão avançar para o checkout") do
  categoriapopup.botãoavançarchechout
  sleep(2)
end

Dado("clico no botão fazer checkout") do
  resumo.realizecheckout
  sleep(3)
end

Dado("clico no botão endereço") do
  endereco.fazercheckout_endereço
  sleep(3)
end

Dado("clico no botão aceito") do
  expedicao.aceito
  sleep(2)
end

Dado("clico no botão fazer expedição") do
  expedicao.btnexpedicao
  sleep(2)
end

Dado("clico no botão transferência") do
  pagamento.transferenciabancaria
  sleep(3)
end

Dado("clico no botão confirmar pedido") do
  confirmarpagamento.confirmarpedido
end